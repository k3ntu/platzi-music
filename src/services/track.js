import platziMusicService from './platzi-music'

const traksService = {}

traksService.search = function (q) {
  const type = 'track'

  return platziMusicService.get('/search', {
    params: { q, type }
  }).then(res => res.data)
}

traksService.getById = function (id) {
  return platziMusicService.get(`/tracks/${id}`)
    .then(res => res.data)
}

export default traksService
